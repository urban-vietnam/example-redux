/***************************************************************
 * for example redux + react navigation
 ***************************************************************/
/** import react */
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { StatusBar } from 'expo-status-bar';
/** import redux */
import { Provider } from 'react-redux';
/** import my store */
import store from './store';

/** import parts for example */
import RootNavigation from './navigation';

/** export Application root */
export default () => (
  // wrap store
  <Provider store={store}>
    <SafeAreaProvider>
      {/* to RootNavigation */}
      <RootNavigation />
      <StatusBar />
    </SafeAreaProvider>
  </Provider>
);
