/***************************************************************
 * for example redux + react navigation store
 ***************************************************************/
/** import redux */
import { combineReducers, createStore } from 'redux';
/** import my reducer */
import tasksReducer, {State as TaskState} from './tasks';

/** export my store type */
export type MyStoreState = {
  tasks: TaskState
}

/** define my reducers */
const myReducers = combineReducers<MyStoreState>({
  tasks: tasksReducer, // for task
  // ... etc reducers
});
/** export my store */
export default createStore(myReducers);