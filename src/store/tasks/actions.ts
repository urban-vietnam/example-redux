/***************************************************************
 * for example Task actions
 ***************************************************************/
/** import my type */
import {Task, ADD_TASK, AddTaskAction, COMPLETED_TASK, CompletedTaskAction} from './type';

/** export action creator ... action creator is to call Action using Dispatch from the screen and communicate it to the Reducer. */
// add task action creator
export const addTask = (title: string, detail: string = ''): AddTaskAction => ({
  type: ADD_TASK,
  payload: {title, detail}
});
// completed task action creator
export const completedTask = (id: Task['id']): CompletedTaskAction => ({
  type: COMPLETED_TASK,
  payload: id,
})