/***************************************************************
 * for example Task reducer (source code pattern = re-duck like)
 ***************************************************************/
/** import my type */
import {Task, Actions, ADD_TASK, AddTaskAction} from './type';

/***************************************************************
 * define typescript
 ***************************************************************/
/** type state for task*/
export type State = Task[];

/***************************************************************
 * define for reducer
 ***************************************************************/
/** define initialstate */
const testInitialState: Task[] = [
  {id: '1', title: '今週にやること', detail: '今週はReduxの勉強を行う', complete: false,},
  {id: '2', title: '今月の目標！', detail: 'Chat Boxの設計書を作ろう！', complete: false,},
];
// __DEV__ = process.env.NODE_ENV !== 'production' => development mode
const initialState: State = process.env.NODE_ENV !== 'production' ? testInitialState : [];

/** export reducer */
export default (state: State = initialState, action: Actions): State => {
  // action 別に実行
  switch (action.type) {
    // case ADD_TASK Action
    case ADD_TASK: {
      // payload is argument of action
      const {title, detail = ''} = (action as AddTaskAction).payload;
      // create next task id
      const nextId = '' + state.length + 1;
      // return now state and new task => new state
      return [ // new state
        ...state // now state
        , {id: nextId, title, detail, complete: false} // new task
      ];
    }
    // else pattern...
    default: return state;
  }
};

