/***************************************************************
 * for task reducers index
 ***************************************************************/
/** import my parts of reducer */
import reducer from './reducer';

/** export reducer */
export type { State } from './reducer';
export default reducer;