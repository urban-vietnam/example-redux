/***************************************************************
 * for example Task reducer common types
 ***************************************************************/
/** state type of task */
export type Task = {
  id: string, // uniq for task
  title: string, // title for task
  detail: string, // detail message for task
  complete: boolean, // true: is Complete / false: is not
};

/** action name(action.type) of task reducer */
// export action types => types is uniq name of your application Reducers. If you use the same action.type, both will work.
export const ADD_TASK = '@MyAppAction/task/addTask'; // add task action の action.typeを定義
export const COMPLETED_TASK = '@MyAppAction/task/completeTask'; // complete task action の action.typeを定義

/** export tasks Actions */
// define type for tasks action
// {
//   type: '@MyAppAction/task/addTask', // uniq name of your application Reducers. If you use the same action.type, both will work.
//   payload: {title: string, detail?: string} // payload is argument for required data of Action function.
// };
export type AddTaskAction = {type: typeof ADD_TASK, payload: {title: string, detail: string}};
// define type for add complete task action
// {
//   type: '@MyAppAction/task/completeTask', // uniq name of your application Reducers
//   payload: Task['id'], // string is id
// }
export type CompletedTaskAction = {type: typeof COMPLETED_TASK, payload: Task['id']};
// export type tasks Actions
export type Actions = AddTaskAction | CompletedTaskAction;