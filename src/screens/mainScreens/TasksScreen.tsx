/***************************************************************
 * tasks screen
 ***************************************************************/
/** import react */
import * as React from 'react';
/** import react native */
import { SafeAreaView, View } from 'react-native';
import { ListItem } from 'react-native-elements';
/** import redux */
import { useSelector } from 'react-redux';
/** import expo */
import { Feather } from '@expo/vector-icons';

/** import for typescript */
import { MyStoreState } from '../../store';
import { Task } from '../../store/tasks/type';

/**
 * タスク 1行
 * @param props 引数
 */
const TaskLine = (props: {task: Task}) => {
  // get task from props
  const [task, setTask] = React.useState<Task>(props.task);
  // render task
  return (
    <ListItem bottomDivider onPress={() => setTask({...task, complete: true})}>
      <Feather name={task.complete ? 'check-square' : 'square'} />
      <ListItem.Content>
          <ListItem.Title>{task.title}</ListItem.Title>
          <ListItem.Subtitle>{task.detail}</ListItem.Subtitle>
      </ListItem.Content>
    </ListItem>
  );
};

/** export screen */
export default () => {
  // get from store
  const tasks = useSelector((state: MyStoreState) => state.tasks);

  return (
    <SafeAreaView>
      <View>
        {/* draw tasks */}
        {tasks.map((t: Task) => <TaskLine key={t.id} task={t} />)}
      </View>
    </SafeAreaView>
  );
};