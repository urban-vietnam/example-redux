/***************************************************************
 * screen index for main nvaigator
 ***************************************************************/
/** import screens */
import TasksScreen from './TasksScreen';
import AddTaskScreen from './AddTaskScreen';

/** export screens */
export {
  TasksScreen
  , AddTaskScreen
}