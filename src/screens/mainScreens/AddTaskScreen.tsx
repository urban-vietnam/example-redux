/***************************************************************
 * add task screen
 ***************************************************************/
/** import react */
import * as React from 'react';

/** import react native */
import { SafeAreaView, View } from 'react-native';
import { Card, Input, Button } from 'react-native-elements';
/** import redux */
import { useDispatch } from 'react-redux';
/** import expo */
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { addTask } from '../../store/tasks/actions';

/** type for Task */
type Task = {
  title: string, // title for task
  detail: string, // detail message for task
};
/**
 * タスク form
 * @param props 引数
 */
const TaskForm = () => {
  // タスク
  const [task, setTask] = React.useState<Task>({title: '', detail: ''});
  // dispatchの具現化
  const dispatch = useDispatch();

  return (
    <Card>
      <Card.Title>New Task</Card.Title>
      <Card.Divider/>
      {/* task form */}
      <Input
        placeholder='task title...'
        leftIcon={<MaterialCommunityIcons name='format-title' size={14} color='black' />}
        onChangeText={(title = '') => setTask({...task, title})}
        inputContainerStyle={{borderWidth:0}}
      />
      <Input
        placeholder='task detail...'
        leftIcon={<MaterialCommunityIcons name='subtitles-outline' size={14} color='black' />}
        onChangeText={(detail = '') => setTask({...task, detail})}
        inputContainerStyle={{borderWidth:0}}
      />
      {/* save task button */}
      <Button
        icon={<Ionicons name='add-circle-outline' color='#ffffff' size={16} style={{marginRight: 15}} />}
        buttonStyle={{borderRadius: 5, marginTop: 14, backgroundColor:'orange'}}
        title='add task'
        // dispatch AddTask Action! => Tasks Reducer subscribes and stores new states.
        onPress={() => dispatch(addTask(task.title, task.detail))}
      />
    </Card>
  );
}

/** export screen */
export default () => {
  return (
    <SafeAreaView>
      <View>
        <TaskForm />
      </View>
    </SafeAreaView>
  );
};