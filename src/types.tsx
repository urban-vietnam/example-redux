/**
 * Root navigator type = Application root screen transitions...
 *  launch the application
 *    ┝ Main ... for example-redux main
 *      ┝ ... and more
 *    └ NotFound ... screen is not everythning else
 */
export type RootParamList = {
  Main: undefined;
  NotFound: undefined;
};

/**
 * Main the application screen transitions...
 *  launch the application
 *    ┝ Main ... for example-redux main
 *      ┝ Tasks ... (initialroute) draw taks list screen
 *      ┝ EditTask ... edit task screen
 *      ┝ AddTask ... create task screen
 *      ┝ ... and more
 */
export type MainParamList = {
  // for task list screen path
  Tasks: undefined;
  // for edit task screen path
  EditTask: undefined;
  // for add task screen path
  AddTask: undefined;
};

// Tasks screen navigator
export type TasksParamList = {
  tasks: undefined;
}

// Tasks screen navigator
export type AddTaskParamList = {
  addTask: undefined;
}