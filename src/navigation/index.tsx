/***************************************************************
 * root navigation this applicatation
 ***************************************************************/
/** import react */
import * as React from 'react';
/** import react navigation */
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer} from '@react-navigation/native';

/** import parts for router */
import LinkingConfiguration from './LinkingConfiguration';
/** import parts for it */
import MainNavigator from './mainNavigator';
import NotFoundScreen from '../screens/NotFoundScreen';

/** import for ts */
import { RootParamList } from '../types';

/***************************************************************
 * parts ... root navigation
 ***************************************************************/
// A root stack navigator is often used for displaying modals on top of all other content
const Stack = createStackNavigator<RootParamList>();
const RootNavigator = () => (
  // draw a screen or navigation to use this time
  <Stack.Navigator 
    initialRouteName="Main"
    screenOptions={{ headerShown: false }}>
    {/* example-redux main  */}
    <Stack.Screen name="Main" component={MainNavigator} />
    {/* everythning else screen */}
    <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
  </Stack.Navigator>
);

/** export root navigation */
export default () => (
  // 「Root navigator」 is used for screen transitions. NavigationContainer is wrapper.
  <NavigationContainer linking={LinkingConfiguration}>
    <RootNavigator />
  </NavigationContainer>
);