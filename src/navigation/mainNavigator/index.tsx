/***************************************************************
 * main navigation this applicatation
 ***************************************************************/
/** import react */
import * as React from 'react';
/** import expo */
import { Ionicons } from '@expo/vector-icons';
/** import react navigation */
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

/** import parts for screen (screenn - navigator = 1 - 1) */
import TasksNavigator from './TasksNavigator';
import AddTaskNavigator from './AddTaskNavigator';
/** import for ts */
import { MainParamList } from '../../types';

/***************************************************************
 * common parts for bottom tab
 ***************************************************************/
/**
 * draw bottom tab bar icon
 * @param props draw this props
 */
const BottomTabBarIcon = (props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) => {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}
/***************************************************************
 * parts ... main navigation
 ***************************************************************/
// sample bottom tab navigator
const MainBottomTab = createBottomTabNavigator<MainParamList>();
// export main navigator
export default () => (
  // define initial route
  <MainBottomTab.Navigator
    initialRouteName="Tasks"
    tabBarOptions={{ activeBackgroundColor:'orange', activeTintColor: 'white' }}
  >
    {/* requires navigator on screen to easily change headers, add child screen, etc... */}
    {/* Each tab has its own navigation stack, you can read more about this pattern here: */}
    {/* https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab */}
    <MainBottomTab.Screen
      name="Tasks"
      component={TasksNavigator}
      options={{
        tabBarIcon: () => <BottomTabBarIcon name="list" color={'dark'} />,
      }}
    />
    <MainBottomTab.Screen
      name="AddTask"
      component={AddTaskNavigator}
      options={{
        tabBarIcon: () => <BottomTabBarIcon name="add" color={'dark'} />,
      }}
    />
  </MainBottomTab.Navigator>
);