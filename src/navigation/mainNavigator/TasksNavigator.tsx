/***************************************************************
 * tasks screen nvaigator
 ***************************************************************/
/** import react */
import * as React from 'react';
/** import react navigation */
import { createStackNavigator } from '@react-navigation/stack';
/** import parts for draw screen */
import { TasksScreen } from '../../screens/mainScreens';
/** import for ts */
import { TasksParamList } from '../../types';

/***************************************************************
 * parts ... tasks navigation
 ***************************************************************/
const TasksStack = createStackNavigator<TasksParamList>();
export default () => (
  <TasksStack.Navigator>
    <TasksStack.Screen
      name="tasks"
      component={TasksScreen}
      options={{ headerTitle: 'Task List' }}
    />
  </TasksStack.Navigator>
);