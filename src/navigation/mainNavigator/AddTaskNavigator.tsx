/***************************************************************
 * add task screen nvaigator
 ***************************************************************/
/** import react */
import * as React from 'react';
/** import react navigation */
import { createStackNavigator } from '@react-navigation/stack';
/** import parts for draw screen */
import { AddTaskScreen } from '../../screens/mainScreens';
/** import for ts */
import { AddTaskParamList } from '../../types';

/***************************************************************
 * parts ... add task navigation
 ***************************************************************/
const AddTaskStack = createStackNavigator<AddTaskParamList>();
export default () => (
  <AddTaskStack.Navigator>
    <AddTaskStack.Screen
      name="addTask"
      component={AddTaskScreen}
      options={{ headerTitle: 'Create Task' }}
    />
  </AddTaskStack.Navigator>
);