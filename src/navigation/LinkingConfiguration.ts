/***************************************************************
 * eample-redu link router confing for web
 ***************************************************************/
/** import expo */
import * as Linking from 'expo-linking';

/** URL config */
export default {
  // prefix ... no definition this time. example https://e-jan.vn/XXX.html => https://e-jan.vn
  prefixes: [Linking.makeUrl('/')],
  // URL path and parent - child relationship of the screen
  config: {
    screens: {
      // parent (root for this application)
      Main :{
        // initial screen.
        initialRouteName: 'Tasks',
        // parent path => /home/xxx(child screen path)
        path: 'home',
        screens: {
          // path = /home/tasks
          Tasks: 'tasks',
          EditTask: {
            // path = /home/task/taks-avs12 => print Task { id : avs12} screen. /home/task/task-new => print new Task { id : '' } screen.
            path: 'task/:id?',
            parse: { id: (id: string = 'new'): string => `task-${id}`,},
            stringify: { id: (id: string = 'new'): string => id.replace(/^user-/, ''), },
          },
          AddTask: 'task/task-new',
        }
      },
      // screen is not everythning else
      NotFound: '*',
    },
  },
};
